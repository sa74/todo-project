
function user(){
    let userName = prompt("Whats your name?")
    const nameShowage = document.getElementById("helloUser")
    nameShowage.innerText = "Hello " + userName
}
user();

function getfetchData(){
    fetch("http://localhost:3000/auth/cookie/tasks"), {
        credentials: "include"
    }
        .then((response) => response.json())
        .then((data) => {

        items = data;
        console.log(items)
        renderList(items);

    });
}

function deleteData(id){
    fetch(`http://localhost:3000/auth/cookie/task/${id}`, {
        credentials: "include",
        method: "DELETE",
        headers: {"Content-Type": "application/json"}})
        .then(response => {
            if(response.ok){
                getfetchData()  
            } else {
                alert("Failed")
            }
        })    
}

function updateData(){
    fetch(`http://localhost:3000/auth/cookie/tasks`,{
        credentials: "include",
        method: "PUT",
        headers: {"Content-Type": "application/json"}})
        .then(response => {
            if(response.ok){
                getfetchData()
                renderList()
            } else {
                alert("Failed")
            }
            renderList();
        })
}

function displayDate(){
    const date = new Date();
    let dateObj = new Date();
    let month = dateObj.getMonth(print) + 1; 
    let day = dateObj.getDate();
    let year = dateObj.getFullYear();
    approxdate = day + "." + month + "." + year;
    const dateDisplay = document.getElementById("date");
    const newDisplay = document.createElement("h1")
    newDisplay.innerText = approxdate;
    dateDisplay.append(newDisplay);
}



let items = [];
function renderList() {
    
    const fullList = document.getElementById("todoList");
    fullList.innerHTML = "";

    items.forEach(function(listItem, i){
        const newLi = document.createElement("li");
        newLi.innerText = listItem.title ?? listItem

        const deleteButton = document.createElement("button")
        deleteButton.innerText = "delete"
        deleteButton.addEventListener("click", function (){
            deleteData(listItem.id);
        })
        newLi.append(deleteButton)

        const editButton = document.createElement("button")
        editButton.innerText = "edit"
        editButton.addEventListener("click", function(){
            items[i] = prompt()
            updateData()
            renderList(items)
        })
        newLi.append(editButton)
        
        fullList.append(newLi)
    })
}


function addTasks(){
    const buttonToDo = document.getElementById("addButton");
    const inputField = document.getElementById("fieldForInput")

    buttonToDo.addEventListener("click", function(){

        const newTask = inputField.value;
        items.push(newTask)
        renderList();
        const itemText = newTask

        fetch("http://localhost:3000/tasks", {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({ title: itemText })
        })
        .then(response => {
            if(response.ok){
                renderList(items)
            } else {
                alert("Failed")
            }
        })
        renderList();

        inputField.value = ""
    })  
}

function displayTaskAmount(){
    let taskAmount = items.length;
    const tasksAmountShowage = document.getElementById("amountOfTasks")
    tasksAmountShowage.innerText = "You got " + taskAmount + " tasks to do";
}

displayDate();
displayTaskAmount();
addTasks();
renderList();


getfetchData();


